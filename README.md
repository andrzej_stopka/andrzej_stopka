## Hello, I'm Andrzej 👋

Welcome to my GitLab profile! I am an aspiring Junior Python Developer who is passionate about learning and building projects from scratch. Let me give you a brief overview of my journey and what I'm currently working on.

## About Me

I am proud to be a participant in the mentoring program called "Za Rączkę". This program focuses on providing guidance and directions on the specific areas of knowledge I need to grasp. Through this program, I receive detailed instructions and tasks that I then diligently work on independently to complete successfully.


## My Projects

I take great pride in the fact that none of my projects are based on tutorials. Every project you see here is a result of my own effort, creativity, and adherence to the provided guidelines. I believe in hands-on learning and strive to develop my skills by tackling real-world challenges.

Feel free to explore these projects for a better understanding of my coding style and problem-solving abilities.

## Learning Python

I am currently dedicated to honing my skills in Python. As an aspiring Junior Python Developer, I consistently seek opportunities to deepen my understanding of the language and expand my knowledge of its various libraries and frameworks. I believe in continuous learning and constantly strive to enhance my coding skills through practice and personal projects.

# Seeking Opportunities

I am actively seeking job opportunities as a Junior Python Developer, where I can contribute my skills and grow further in a collaborative and dynamic environment. If you have any relevant opportunities or would like to connect, please don't hesitate to reach out to me.

Thank you for visiting my profile! Feel free to connect with me or explore my projects. I'm always open to collaboration and new learning experiences.

